package es.upm.emse.absd.behaviours;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class OneToOnePlayBehaviourTest {

    @Test void testPlay(){
        assertTrue(new OneToOnePlayBehaviour(null).playf("Rock", "Scissors"));
        assertTrue(new OneToOnePlayBehaviour(null).playf("Rock", "Rock"));
        assertFalse(new OneToOnePlayBehaviour(null).playf("Rock", "Paper"));
        assertTrue(new OneToOnePlayBehaviour(null).playf("Scissors", "Scissors"));
        assertTrue(new OneToOnePlayBehaviour(null).playf("Scissors", "Paper"));
        assertFalse(new OneToOnePlayBehaviour(null).playf("Scissors", "Rock"));
        assertTrue(new OneToOnePlayBehaviour(null).playf("Paper", "Rock"));
        assertTrue(new OneToOnePlayBehaviour(null).playf("Paper", "Paper"));
        assertFalse(new OneToOnePlayBehaviour(null).playf("Paper", "Scissors"));
    }

}