package es.upm.emse.absd.behaviours;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class OneToManyPlayBehaviourTest {

    @Test void testPlay(){
        assertTrue(new OneToManyPlayBehaviour(null).playf("Rock", "Scissors"));
        assertTrue(new OneToManyPlayBehaviour(null).playf("Rock", "Rock"));
        assertFalse(new OneToManyPlayBehaviour(null).playf("Rock", "Paper"));
        assertTrue(new OneToManyPlayBehaviour(null).playf("Scissors", "Scissors"));
        assertTrue(new OneToManyPlayBehaviour(null).playf("Scissors", "Paper"));
        assertFalse(new OneToManyPlayBehaviour(null).playf("Scissors", "Rock"));
        assertTrue(new OneToManyPlayBehaviour(null).playf("Paper", "Rock"));
        assertTrue(new OneToManyPlayBehaviour(null).playf("Paper", "Paper"));
        assertFalse(new OneToManyPlayBehaviour(null).playf("Paper", "Scissors"));
    }

}