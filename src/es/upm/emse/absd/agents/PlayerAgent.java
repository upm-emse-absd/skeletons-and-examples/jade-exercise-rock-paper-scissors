package es.upm.emse.absd.agents;

import es.upm.emse.absd.Utils;
import es.upm.emse.absd.behaviours.OneToManyPlayBehaviour;
import es.upm.emse.absd.behaviours.OneToOnePlayBehaviour;
import jade.core.Agent;

public class PlayerAgent extends Agent {

    public static String SERVICE_MEMBER = "player";

    protected void setup()
    {
        if (Utils.register(this, PlayerAgent.SERVICE_MEMBER)) {
            OneToOnePlayBehaviour pb = new OneToOnePlayBehaviour(this);
            addBehaviour(pb);
        }
        else doDelete();
    }

}
