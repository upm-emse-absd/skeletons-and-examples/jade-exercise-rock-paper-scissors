package es.upm.emse.absd.behaviours;

import es.upm.emse.absd.Utils;
import es.upm.emse.absd.agents.PlayerAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Log
public class OneToOnePlayBehaviour extends SequentialBehaviour {

    private int round = 1;

    public OneToOnePlayBehaviour(Agent a) {
        super(a);

        List<String> words = Arrays.asList("Rock", "Paper", "Scissors");
        Random rand = new Random();

        addSubBehaviour(new TickerBehaviour(a, 3000) {

            @Override
            protected void onTick() {

                AID[] players = Utils.searchDF(this.getAgent(), PlayerAgent.SERVICE_MEMBER);

                if (players != null && players.length==2) {
                    String randomWord = words.get(rand.nextInt(words.size()));
                    log.info("[round " + round + " (" + players.length +  " players)] " + this.getAgent().getLocalName() + ": Rock, Paper, Scissors! I choose " + randomWord);
                    sendDecision(players, randomWord);
                    stillPlaying(players, randomWord);
                }
                else {
                    log.info("[round " + round + "] " + this.getAgent().getLocalName() + ": I won!!!");
                    a.doSuspend();
                }
            }

        });

    }

    private void sendDecision(AID[] players, String randomWord) {
        for (AID rival : players)
            if (!rival.getLocalName().equals(this.getAgent().getLocalName()))
                this.getAgent().send(Utils.newMsg(this.getAgent(), ACLMessage.INFORM, randomWord, rival));
    }

    private void stillPlaying(AID[] players, String randomWord) {
        boolean keepPlaying = play(randomWord, this.getAgent().blockingReceive().getContent());
        if (!keepPlaying) {
            Utils.deregister(this.getAgent());
            log.info("[round " + round + " (" + players.length +  " players)] " + this.getAgent().getLocalName() + ": I lost...");
            this.getAgent().doSuspend();
        }
        round++;
    }

    protected boolean play(String me, String you) {
        return me.equals(you)
            || me.equals("Rock") && you.equals("Scissors")
            || me.equals("Scissors") && you.equals("Paper")
            || me.equals("Paper") && you.equals("Rock");
    }

}

