package es.upm.emse.absd.behaviours;

import es.upm.emse.absd.Utils;
import es.upm.emse.absd.agents.PlayerAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Log
public class OneToManyPlayBehaviour extends SequentialBehaviour {

    private AID[] rivals;
    private int round = 1;

    public OneToManyPlayBehaviour(Agent a) {
        super(a);

        List<String> words = Arrays.asList("Rock", "Paper", "Scissors");
        Random rand = new Random();

        addSubBehaviour(new TickerBehaviour(a, 3000) {

            @Override
            protected void onTick() {

                ACLMessage msg = null;
                Map<String, Integer> counts = new HashMap<>();
                rivals = Utils.searchDF(this.getAgent(), PlayerAgent.SERVICE_MEMBER);

                if (rivals != null && rivals.length>1) {
                    String randomWord = words.get(rand.nextInt(words.size()));
                    log.info("[round " + round + " (" + rivals.length +  " players)] " + this.getAgent().getLocalName() + ": Rock, Paper, Scissors! I choose " + randomWord);

                    for (AID rival : rivals)
                        a.send(Utils.newMsg(this.getAgent(), ACLMessage.INFORM, randomWord, rival));

                    // Traverse through array elements and count frequencies
                    int remainingRivals = rivals.length;
                    while (remainingRivals > 0) {
                        msg = a.blockingReceive();
                        String s = msg.getContent();
                        if (counts.containsKey(s))
                            counts.put(s, counts.get(s) + 1);
                        else
                            counts.put(s, 1);
                        remainingRivals--;
                    }

                    boolean keepPlaying =
                        rivals.length==2 ? playf(randomWord, msg.getContent()) : playn(randomWord, counts);

                    if (!keepPlaying) {
                        Utils.deregister(this.getAgent());
                        log.info("[round " + round + " (" + rivals.length +  " players)] " + this.getAgent().getLocalName() + ": I lost...");
                        a.doSuspend();
                    }

                    round++;
                }
                else {
                    log.info("[round " + round + "] " + this.getAgent().getLocalName() + ": I won!!!");
                    a.doSuspend();
                }
            }
        });

    }

    private boolean playn(String me, Map<String,Integer> others) {
        // Return most frequent
        String winner = Collections.max(others.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
        return me.equals(winner);
    }

    protected boolean playf(String me, String you) {
        return me.equals(you)
            || me.equals("Rock") && you.equals("Scissors")
            || me.equals("Scissors") && you.equals("Paper")
            || me.equals("Paper") && you.equals("Rock");
    }

}

