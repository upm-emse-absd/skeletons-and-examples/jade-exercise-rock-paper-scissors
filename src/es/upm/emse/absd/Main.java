package es.upm.emse.absd;

import static es.upm.emse.absd.Utils.ANSI_WHITE;

import es.upm.emse.absd.agents.PlayerAgent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.StaleProxyException;

/**
 * @author      Jose Maria Barambones <j.barambones@upm.es>
 * @version     1.2.2
 *
 * Main Class.
 */
public class Main {

    private static jade.wrapper.AgentContainer cc;

    public static final boolean log = false;

    // Executed from Singletons.
    private static void loadBoot(){

        // Get a hold on JADE runtime
        jade.core.Runtime rt = jade.core.Runtime.instance();

        // Exit the JVM when there are no more containers around
        rt.setCloseVM(true);
        System.out.println("Runtime created");

        // Create a default profile
        Profile profile = new ProfileImpl(null, 1200, null);
        //profile.setParameter("verbosity","5");
        System.out.println("Profile created");

        System.out.println("Launching a whole in-process platform..."+profile);


        try {
            cc = rt.createMainContainer(profile);

            System.out.println("Launching the rma agent on the main container ...");
            cc.createNewAgent("rma","jade.tools.rma.rma", new Object[0]).start();

        } catch (StaleProxyException e) {
            System.err.println("Error during boot!!!");
            e.printStackTrace();
            System.exit(1);
        }

        // now set the default Profile to start a container
        ProfileImpl agentContainerProfile = new ProfileImpl(null, 1200, null);
        //System.out.println("Launching the agent container ..."+agentContainerProfile);

        cc = rt.createAgentContainer(agentContainerProfile);
        //System.out.println("Launching the agent container after ..."+agentContainerProfile);
        System.out.println("Agent Container created");

    }

    private static void loadPlayers(int numPlayers) {
        try {
            for (int n = 0; n<numPlayers; n++)
                cc.createNewAgent("P" + n, PlayerAgent.class.getName(), new Object[]{"0"}).start();

        } catch (StaleProxyException e) {
            System.err.println("Error creating agent!!!");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void printHelp() {
        System.out.println("""
            ---------------------------------------------------------------
            --- JADE exercise - Rock, Paper, Scissors! v2 (multi-agent) ---
            ---------------------------------------------------------------
            Agents:
                + PlayerAgent: Agent player.
            Parameters:
                + Number of players.
            """

        );
    }

    public static void main(String[] args) {

        System.setProperty("java.util.logging.SimpleFormatter.format", ANSI_WHITE + "[%1$tF %1$tT] [%4$-7s] %5$s %n");

        if (args.length != 1) {
            printHelp();
            System.exit(0);
        }

        System.out.println("Starting...");
        loadBoot();
        System.out.println("MAS loaded...");
        loadPlayers(Integer.parseInt(args[0]));

    }

}
